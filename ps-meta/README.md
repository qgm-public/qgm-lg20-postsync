# Postsync Meta Skripte

Diese Skripte haben keine Funktion beim eigentlichen Postsync Vorgang, sie
befinden sich hier, weil sie hifreich sind/sein koennen - und damit sie in der
Versionsverwaltung mitversioniert werden.

## Inhalt 

* `xubuntu1804.cloop.postsync`: Das Basis-Postsync-Skript, damit dieses Postsync-Verzeichnis funktioniert. 
* `get-tools.sh` : Damit wird ein Git Repo ins Home des Vorlagenbenutzers geklont, in dem man Werkzeuge "nachschieben" kann, z.B. zur Wiederherstellung von Konfigurationseinstellungen oder zur Fernwartung. 
* `lg20.cfg`: Passende grub Config   
* `start.conf.lg20`: Passende start.conf


