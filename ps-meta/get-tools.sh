#!/bin/bash

if [ ! -d ./home/linuxadmin/ ];then 
    echo "Dieses Skript muss aus dem Basisverzeichnis der Postsync Patches aufgerufen werden"
    echo "Also:  ./ps-meta/get-tools.sh"
    exit 1
fi

git clone https://codeberg.org/qgm-public/qgm-laptop-manager.git home/linuxadmin/.qgm-offlineconfig > /dev/null 2>&1
git pull home/linuxadmin/.qgm-offlineconfig > /dev/null 2>&1
