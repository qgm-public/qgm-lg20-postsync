#!/bin/sh

SYSPATH="/sys/class/backlight/intel_backlight"
BLFILE="/sys/class/backlight/intel_backlight/brightness"
RESOLUTION=20
MIN=5

if [ ! -d $SYSPATH ]; then 
	echo "Is this really a T440s?"
	echo "No directory $SYSPATH found."
	exit 0
fi

MAX=$(cat $SYSPATH/max_brightness)
ACTUAL=$(cat $SYSPATH/actual_brightness)
STEPSIZE=$(expr $MAX / $RESOLUTION )


if [ "x$1" = "x" ]; then 
	echo "Brightness: $ACTUAL/$MAX."
	echo "Stepsize: $STEPSIZE."
fi

if [ "x$1" = "xup" ]; then
	NEW=$(($(cat $BLFILE)+$STEPSIZE)) 
	if [ $NEW -gt $MAX ]; then 
		NEW=$MAX
	fi
	echo $NEW > $BLFILE

fi	

if [ "x$1" = "xdown" ]; then
	NEW=$(($(cat $BLFILE)-$STEPSIZE)) 
	if [ $NEW -lt $MIN ]; then 
		NEW=$MIN
	fi
	echo $NEW > $BLFILE

fi	


