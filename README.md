# qgm-lg20-postsync

Dieses Repository enthält die postsync Anpassungen für linbo, um das Standard-xUbuntu-18.04-Image des QG Mössingen für Leihlaptops anzupassen. 

Entwickelt wurden die Skripte auf Lenovo-T440s.

Weitere Infos finden sich im Wiki https://codeberg.org/qgm-public/qgm-lg20-postsync/wiki/Home